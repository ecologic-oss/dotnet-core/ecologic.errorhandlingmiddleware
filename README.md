# EcoLogic.ErrorHandlingMiddleware - Error Handling Middleware for ASP.NET Core

EcoLogic.ErrorHandlingMiddleware allows intercept unhandeled exceptions in your application and return a predefined error message, instead of the stacktrace.
You can choose in which environments (Production, Staging, Development) the middleware will run.

## Using the middleware

```csharp
//...
using EcoLogic.ErrorHandlingMiddleware;


namespace sp.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
           //...
           services.AddErrorHandlingMiddleware(o =>
            {
                o.ErrorMessage = "An error occurred whilst processing your request. Please contact the system administrator.";
                o.UseInProduction = true;
                o.UseInStaging = true;
                o.UseInDevelopment = false;
            });

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger, IServiceProvider serviceProvider)
        {

            //...
            app.UseErrorHandlingMiddleware();
            app.UseMvc();
        }
    }
}
```

The above configuration would produce the following (json) error within staging and production:

```json
{
    "status": 500,
    "message": "An error occurred whilst processing your request. Please contact the system administrator."
}
```

## NotFoundException

If we want tho return a 404 to the caller of the request, we can throw a `NotFoundException` anywhere within our code.

```csharp

public void UpdateStudent(Student student) {
    if (student == null) {
        throw new NotFoundException("Student could not be found.");
    }
    // ...
}

```

Which would produce the following (json) error in all environments:

```json
{
    "status": 404,
    "message": "Student could not be found."
}
```
