using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using EcoLogic.ErrorHandlingMiddleware.Exceptions;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Xunit;

namespace EcoLogic.ErrorHandlingMiddleware.Tests
{
    public class ErrorHandlingMiddlewareTests
    {
        private readonly DefaultHttpContext _context;

        public ErrorHandlingMiddlewareTests()
        {
            _context = new DefaultHttpContext();
            _context.Response.Body = new MemoryStream();
            _context.Request.Method = "GET";
            _context.Request.Host = new HostString("example.com");
            _context.Request.PathBase = new PathString("/route");
            _context.Request.Path = new PathString("/endpoint");
            _context.Request.QueryString = new QueryString("?foo=bar");
            _context.Request.Scheme = "http";
        }

        [Fact]
        public async Task WhenACustomExceptionIsRaised_ErrorHandlingMiddlwareShouldHandleItToCustomErrorResponse()
        {
            // Arrange
            const string middlewareErrorMessage = "Middleware Error Message";
            var middleware = new ErrorHandlingMiddleware(
                innerHttpContext => throw new ArgumentException("A non captured Error occured..."),
                NullLogger<ErrorHandlingMiddleware>.Instance,
                new HostingEnvironment {EnvironmentName = "Production"},
                Options.Create(new ErrorHandlingMiddlewareOptions
                {
                    ErrorMessage = middlewareErrorMessage,
                    UseInProduction = true
                })
            );

            //Act
            await middleware.InvokeAsync(_context);
            _context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(_context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseMessage = JsonConvert.DeserializeObject<ErrorMessage>(streamText);

            //Assert
            Assert.Equal(middlewareErrorMessage, responseMessage.Message);
        }

        [Fact]
        public async Task WhenACustomExceptionIsRaised_ErrorHandlingMiddlwareShouldUseCustomErrorCode()
        {
            // Arrange
            const int middlwareErrorCode = 500;
            var middleware = new ErrorHandlingMiddleware(
                innerHttpContext => throw new ArgumentException("A non captured Error occured..."),
                NullLogger<ErrorHandlingMiddleware>.Instance,
                new HostingEnvironment {EnvironmentName = "Production"},
                Options.Create(new ErrorHandlingMiddlewareOptions
                {
                    ErrorCode = middlwareErrorCode,
                    UseInProduction = true
                })
            );

            //Act
            await middleware.InvokeAsync(_context);
            _context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(_context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseMessage = JsonConvert.DeserializeObject<ErrorMessage>(streamText);

            //Assert
            Assert.Equal(middlwareErrorCode, responseMessage.Status);
            Assert.Equal(middlwareErrorCode, _context.Response.StatusCode);
        }

        [Fact]
        public async Task WhenACustomExceptionIsRaised_ErrorHandlingMiddlwareShouldPasstroughTheExceptionIfGenericExpceptionWasNotEnableForThisEnvironment()
        {
            // Arrange
            const string noncaptureExceptionMessage = "A non captured Error occured...";
            var middleware = new ErrorHandlingMiddleware(
                innerHttpContext => throw new ArgumentException(noncaptureExceptionMessage),
                NullLogger<ErrorHandlingMiddleware>.Instance,
                new HostingEnvironment {EnvironmentName = "Production"},
                Options.Create(new ErrorHandlingMiddlewareOptions
                {
                    UseInProduction = false
                })
            );

            //Act
            await middleware.InvokeAsync(_context);
            _context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(_context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseMessage = JsonConvert.DeserializeObject<ErrorMessage>(streamText);

            //Assert
            Assert.Equal(noncaptureExceptionMessage, responseMessage.Message);
        }

        [Fact]
        public async Task WhenANotFoundExceptionIsRaised_ErrorHandlingMiddlwareShouldReturn404()
        {
            // Arrange
            const string notfoundError = "Student with Id: 11 could not be found.";
            var middleware = new ErrorHandlingMiddleware(
                innerHttpContext => throw new NotFoundException(notfoundError),
                NullLogger<ErrorHandlingMiddleware>.Instance,
                new HostingEnvironment {EnvironmentName = "Production"},
                Options.Create(new ErrorHandlingMiddlewareOptions
                {
                    UseInProduction = true
                })
            );

            //Act
            await middleware.InvokeAsync(_context);
            _context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(_context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseMessage = JsonConvert.DeserializeObject<ErrorMessage>(streamText);

            //Assert
            Assert.Equal((int) HttpStatusCode.NotFound, responseMessage.Status);
        }

        [Fact]
        public async Task WhenANotFoundExceptionIsRaised_ErrorHandlingMiddlwareShouldReturnExceptionErrorMessage()
        {
            // Arrange
            const string notfoundError = "Student with Id: 11 could not be found.";
            var middleware = new ErrorHandlingMiddleware(
                innerHttpContext => throw new NotFoundException(notfoundError),
                NullLogger<ErrorHandlingMiddleware>.Instance,
                new HostingEnvironment {EnvironmentName = "Production"},
                Options.Create(new ErrorHandlingMiddlewareOptions
                {
                    UseInProduction = true
                })
            );

            //Act
            await middleware.InvokeAsync(_context);
            _context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(_context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseMessage = JsonConvert.DeserializeObject<ErrorMessage>(streamText);

            //Assert
            Assert.Equal(notfoundError, responseMessage.Message);
        }

        [Fact]
        public async Task WhenANotFoundExceptionIsRaised_ErrorHandlingMiddlwareShouldReturnStatusCodeFromOptionsIfItsSet()
        {
            // Arrange
            const string notfoundError = "Student with Id: 11 could not be found.";
            var middleware = new ErrorHandlingMiddleware(
                innerHttpContext => throw new NotFoundException(notfoundError),
                NullLogger<ErrorHandlingMiddleware>.Instance,
                new HostingEnvironment {EnvironmentName = "Production"},
                Options.Create(new ErrorHandlingMiddlewareOptions
                {
                    UseInProduction = true,
                    NotFoundCode = 666
                })
            );

            //Act
            await middleware.InvokeAsync(_context);
            _context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(_context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseMessage = JsonConvert.DeserializeObject<ErrorMessage>(streamText);

            //Assert
            Assert.Equal(666, responseMessage.Status);
        }
    }
}