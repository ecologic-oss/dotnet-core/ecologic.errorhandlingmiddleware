using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace EcoLogic.ErrorHandlingMiddleware
{
    public static class ErrorHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorHandlingMiddleware>();
        }

        public static IServiceCollection AddErrorHandlingMiddleware(this IServiceCollection service, Action<ErrorHandlingMiddlewareOptions> options)
        {
            options = options ?? (opts => { });
            service.Configure(options);
            return service;
        }
    }
}