namespace EcoLogic.ErrorHandlingMiddleware
{
    public sealed class ErrorHandlingMiddlewareOptions
    {
        public bool UseInProduction { get; set; }
        public bool UseInStaging { get; set; }
        public bool UseInDevelopment { get; set; }
        public string ErrorMessage { get; set; }
        public int? ErrorCode { get; set; }
        public int? NotFoundCode { get; set; }
    }
}