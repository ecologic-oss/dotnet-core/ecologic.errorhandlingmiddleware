namespace EcoLogic.ErrorHandlingMiddleware
{
    public sealed class ErrorMessage
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}