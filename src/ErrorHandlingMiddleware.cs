﻿using System;
using System.Net;
using System.Threading.Tasks;
using EcoLogic.ErrorHandlingMiddleware.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace EcoLogic.ErrorHandlingMiddleware
{
    public sealed class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;
        private readonly IHostingEnvironment _env;
        private readonly ErrorHandlingMiddlewareOptions _options;
        private const string GeneralErrorMessage = "An error occurred whilst processing your request. Please contact the system administrator.";

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger, IHostingEnvironment env, IOptions<ErrorHandlingMiddlewareOptions> options)
        {
            _next = next;
            _logger = logger;
            _env = env;
            _options = options.Value;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                var requestDetails = $"{context.Request?.Method}: {context.Request?.GetDisplayUrl()}";
                _logger.LogError($"{requestDetails}\nException: {ex}");
                await HandleExceptionAsync(context, ex, _env);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception, IHostingEnvironment env)
        {
            context.Response.ContentType = "application/json";
            var lowerCaseSerializerSettings = new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()};
            string result;

            if (exception is NotFoundException)
            {
                context.Response.StatusCode = _options.NotFoundCode ?? (int) HttpStatusCode.NotFound;
                result = JsonConvert.SerializeObject(new ErrorMessage
                {
                    Status = context.Response.StatusCode,
                    Message = exception.Message
                }, lowerCaseSerializerSettings);
            }
            else
            {
                context.Response.StatusCode = _options.ErrorCode ?? (int) HttpStatusCode.InternalServerError;
                result = env.IsProduction() && _options.UseInProduction ||
                         env.IsStaging() && _options.UseInStaging ||
                         env.IsDevelopment() && _options.UseInDevelopment
                    ? JsonConvert.SerializeObject(new ErrorMessage
                    {
                        Status = context.Response.StatusCode,
                        Message = string.IsNullOrEmpty(_options.ErrorMessage) ? GeneralErrorMessage : _options.ErrorMessage
                    }, lowerCaseSerializerSettings)
                    : JsonConvert.SerializeObject(new ErrorMessage
                    {
                        Status = context.Response.StatusCode,
                        Message = exception.Message
                    }, lowerCaseSerializerSettings);
            }

            return context.Response.WriteAsync(result);
        }
    }
}